package exof.skylarkj.lhdfa;

/**
 * Transition to request leave current level lhdfa.
 * @author Rahnoc
 */
public class BreakTransition implements ITransition {
	public static final String BREAK_TRANS_NAME = "__lhdfa_break_";
	
	/**
	 * Constructor.
	 */
	public BreakTransition(){
		//
	}
	
	@Override
	public String getName() {
		return BREAK_TRANS_NAME;
	}

}
