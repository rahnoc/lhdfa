package exof.skylarkj.lhdfa;

public interface IState {
	//String getURI();
	String getName();
	void setParent(LHDFA lhdfa);
	LHDFA getParent();
	void onEnter();
	ITransition activate();
	void lock();
	void onExit();
}
