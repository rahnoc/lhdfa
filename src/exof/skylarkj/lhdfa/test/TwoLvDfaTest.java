package exof.skylarkj.lhdfa.test;

import exof.skylarkj.lhdfa.KeyLockedWaker;
import exof.skylarkj.lhdfa.LHDFA;
import exof.skylarkj.lhdfa.State;
import exof.skylarkj.lhdfa.TimeLockedWaker;

/**
 * A two layers LHDFA example.
 * 
 * [outer]
 *  >Case1
 *  >MWait1
 *  >Case2
 *  >[inner]
 *      >Case1
 *      >MWait1
 *      >Case2
 *      >MWait2
 *      >Case3
 *  >Case3
 *  
 * 
 * 
 * @author Rahnoc
 */
public class TwoLvDfaTest {
	private LHDFA dfaInner;
	private LHDFA dfaOuter;
	private KeyLockedWaker klw3 = new KeyLockedWaker("3kLW", 3);
	
	public static void main(String[] args) {
		TwoLvDfaTest test = new TwoLvDfaTest();
		test.run();
	}
	
	
	/**
	 * A background thread running after test start.
	 * @author Rahnoc
	 */
	public class BackgroundThread implements Runnable{
		@Override
		public void run() {
			while(true){
				//System.out.println(" - BG Thread - ");
				try {
					Thread.sleep(2000);
					//Keep insert key when in conscious.
					klw3.insertKey();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	/**
	 * Two main threads in this test.
	 * 1. LHDFA thread, for lhdfa logic jobs.
	 * 2. BG thread, sleep -> insert a key -> sleep ->...
	 */
	private void run(){
		this.dfaInner = this.initInnerDFA();
		this.dfaOuter = this.initOuterDFA( this.dfaInner );
		
		//prepare background thread.
		Thread bgt = new Thread( new BackgroundThread());
		bgt.start();
		
		//The lhdfa thread.
		this.dfaOuter.start();
	}
	
	/**
	 * Initialize a single layer LHDFA without using LhdfaBuilder..
	 * 
	 */
	private LHDFA initInnerDFA(){
		LHDFA dfa = new LHDFA("inner");
		
		//Add states.
		dfa.addState( new State("Case1") );
		dfa.addState( new State("MWait1") );
		dfa.addState( new State("Case2") );
		dfa.addState( new State("MWait2") );
		dfa.addState( new State("Case3") );
		dfa.addState( new State("Case4") );
		
		//Set transition map.
		//dfa.addTransition("Case1", "next", "Case4");
		dfa.addTransition("Case1", "next", "MWait1");
		dfa.addTransition("MWait1", "next", "Case2");
		dfa.addTransition("Case2", "next", "MWait2");
		dfa.addTransition("MWait2", "next", "Case3");
		
		//Set init & end states.
		dfa.setInitailState( "Case1" );
		dfa.setEndState( "Case3" );
		
		//Bind logic implementation.
		dfa.setLogicImpl( new SingleLvDfaLogic(dfa) );
		
		//Add waker.
		TimeLockedWaker tlw = new TimeLockedWaker("timeLW");
		dfa.addLockedWaker(tlw);
		//
		dfa.addLockedWaker(klw3);
		
		return dfa;
	}
	
	/**
	 * 
	 * 
	 */
	private LHDFA initOuterDFA(LHDFA iDfa){
		LHDFA dfa = new LHDFA("outer");
		
		//Add states.
		dfa.addState( new State("Case1") );
		dfa.addState( new State("MWait1") );
		dfa.addState( new State("Case2") );
		dfa.addState( iDfa );
		dfa.addState( new State("Case3") );
		
		//Set transition map.
		dfa.addTransition("Case1", "next", "MWait1");
		dfa.addTransition("MWait1", "next", "Case2");
		dfa.addTransition("Case2", "next", "inner");
		dfa.addTransition("inner", "exit", "Case3");
		
		//Set init & end states.
		dfa.setInitailState( "Case1" );
		dfa.setEndState( "Case3" );
		
		//Bind logic implementation.
		dfa.setLogicImpl( new SingleLvDfaLogic(dfa) );
		
		//Add waker.
		TimeLockedWaker tlw = new TimeLockedWaker("timeLW");
		dfa.addLockedWaker(tlw);
		//
		dfa.addLockedWaker(klw3);
		
		return dfa;
	}
}
