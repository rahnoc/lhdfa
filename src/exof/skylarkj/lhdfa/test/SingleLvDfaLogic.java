package exof.skylarkj.lhdfa.test;

import exof.skylarkj.lhdfa.BreakTransition;
import exof.skylarkj.lhdfa.DelayedTransition;
import exof.skylarkj.lhdfa.HaltTransition;
import exof.skylarkj.lhdfa.IDfaLogic;
import exof.skylarkj.lhdfa.IState;
import exof.skylarkj.lhdfa.ITransition;
import exof.skylarkj.lhdfa.KeyLockedWaker;
import exof.skylarkj.lhdfa.LHDFA;
import exof.skylarkj.lhdfa.TimeLockedWaker;

/**
 * Implementation of a simple sequential logic cases.
 * 
 * In this implementation, two type of states used:
 * 1. Normal state:
 *     > transition blocked by a 3s delay.
 * 2. Multi-wait state:
 *     > Transition blocked by a 3-slots lockWaker.
 * 
 * Flow:
 * =>  [Case1] -3s-> [MWait1] -|||-> [Case2] -3s-> [MWait2] -|||-> [Case3] -3s-> =>
 * 
 * -3s-> 3 seconds delay.
 * -|||-> three slots lock.
 * 
 * Any state return BreakTransition will make it's parent LHDFA to exit.
 * 
 * @author Rahnoc
 */
public class SingleLvDfaLogic implements IDfaLogic{
	LHDFA lhdfa;
	
	public SingleLvDfaLogic(LHDFA lhdfa){
		this.lhdfa = lhdfa;
	}
	
	@Override
	public ITransition exec(IState state) {
		switch(state.getName()){
		//Delayed exit state.
		case "Case1":
		case "Case2":
		case "Case3":
			return this.caseActivate(state);
		//Break exit state.
		case "Case4":
			return new BreakTransition();
		
		
		//Multi-locks state.
		case "MWait1":
		case "MWait2":
			return this.mWaitActivate(state);
			
		//LHDFA
		case "outer":
		case "inner":
			return this.lhdfaActivate(state);
			
		default:
			System.out.println("default case: " + state.getName());
			break;
		}
		
		return null;
	}
	
	/**
	 * Logic for a normal state.
	 * Choose lockedWaker as TimeLockedWaker, and return a DelayedTransition.
	 * @param state
	 * @return
	 */
	private ITransition caseActivate(IState state){
		System.out.println("[" + state.getParent().getURI() + "]:" + state.getName() + " activate");
		long delay = 3000;
		
		//Prepare waker.
		TimeLockedWaker tlw = (TimeLockedWaker) this.lhdfa.findLockedWaker("timeLW");
		tlw.setTime( delay );
		this.lhdfa.setCurrentLockedWaker( tlw );
		
		//Prepare transition.
		return new DelayedTransition("next", delay);
		//return new BreakTransition();
	}
	
	/**
	 * Logic for a multi-wait state.
	 * Choose lockedWaker as KeyLockedWaker, and return a HaltTransition.
	 * @param state
	 * @return
	 */
	private ITransition mWaitActivate(IState state){
		System.out.println("[" + state.getParent().getURI() + "]:" + state.getName() + " activate");
		
		//Prepare waker.
		KeyLockedWaker klw = (KeyLockedWaker) this.lhdfa.findLockedWaker("3kLW");
		this.lhdfa.setCurrentLockedWaker( klw );
		
		//Prepare transition.
		return new HaltTransition("next");
	}
	
	/**
	 * Logic for lhdfa node.
	 * Choose no lockedWaker, and return a DelayedTransition.
	 * @param state
	 * @return
	 */
	private ITransition lhdfaActivate(IState state){
		System.out.println("[" + ((LHDFA)state).getURI() + "] activate");
		
		//
		this.lhdfa.setCurrentLockedWaker( null );
		
		//Prepare transition.
		return new DelayedTransition("exit", 0);
	}
}
