package exof.skylarkj.lhdfa.test;

import exof.skylarkj.lhdfa.KeyLockedWaker;
import exof.skylarkj.lhdfa.LHDFA;
import exof.skylarkj.lhdfa.State;
import exof.skylarkj.lhdfa.TimeLockedWaker;

/**
 * Single level LHDFA test.
 * Demonstrate a LHDFA implementation without nest structure.
 * LHDFA is built barely manually (not by the builder).
 * Test the basic thread flow.
 * 
 * @author Rahnoc
 */
public class SingleLvDfaTest {
	private LHDFA dfa;
	private KeyLockedWaker klw3 = new KeyLockedWaker("3kLW", 3);
	
	public static void main(String[] args) {
		SingleLvDfaTest test = new SingleLvDfaTest();
		test.run();
	}
	
	/**
	 * A background thread running after test start.
	 * @author Rahnoc
	 */
	public class BackgroundThread implements Runnable{
		@Override
		public void run() {
			while(true){
				System.out.println(" - BG Thread - ");
				try {
					Thread.sleep(2000);
					//Keep insert key when in conscious.
					klw3.insertKey();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Two main threads in this test.
	 * 1. LHDFA thread, for lhdfa logic jobs.
	 * 2. BG thread, sleep -> insert a key -> sleep ->...
	 */
	private void run(){
		this.dfa = this.initDFA();
		
		//prepare background thread.
		Thread bgt = new Thread( new BackgroundThread());
		bgt.start();
		
		//The lhdfa thread.
		this.dfa.start();
	}
	
	/**
	 * Initialize a single layer LHDFA without using LhdfaBuilder..
	 * 
	 */
	private LHDFA initDFA(){
		LHDFA dfa = new LHDFA("dfa_1");
		
		//Add states.
		dfa.addState( new State("Case1") );
		dfa.addState( new State("MWait1") );
		dfa.addState( new State("Case2") );
		dfa.addState( new State("MWait2") );
		dfa.addState( new State("Case3") );
		
		//Set transition map.
		dfa.addTransition("Case1", "next", "MWait1");
		dfa.addTransition("MWait1", "next", "Case2");
		dfa.addTransition("Case2", "next", "MWait2");
		dfa.addTransition("MWait2", "next", "Case3");
		
		//Set init & end states.
		dfa.setInitailState( "Case1" );
		dfa.setEndState( "Case3" );
		
		//Bind logic implementation.
		dfa.setLogicImpl( new SingleLvDfaLogic(dfa) );
		
		//Add waker.
		TimeLockedWaker tlw = new TimeLockedWaker("timeLW");
		dfa.addLockedWaker(tlw);
		//
		dfa.addLockedWaker(klw3);
		
		return dfa;
	}
}
