package exof.skylarkj.lhdfa;

/**
 * Logic interface of LHDFA.
 * @author Rahnoc
 */
public interface IDfaLogic {
	
	/**
	 * Implementation of state's active method.
	 * Remember to indicate lockedWaker by ldhdfa.setCurrentLockedWaker(), and
	 * return applied Transition.
	 * @param state
	 * @return
	 */
	ITransition exec(IState state);
}
