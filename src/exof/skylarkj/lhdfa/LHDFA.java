package exof.skylarkj.lhdfa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Lazy Hierarchical Deterministic Finite Automata.
 * {S, i, e, s:t->n}
 * S: states used.
 * i: initial state.
 * e: end state.
 * s:t->n: f(state,transition)->next state.
 * 
 * Used to simulate a switch-case logic structure with lock-wait mechanism.
 * Has a thread starter for the case when this is the top most of the automata.
 * 
 * @author Rahnoc
 */
public class LHDFA implements IState{
	//DFA properties.
	String name;
	private String URI;
	LHDFA parent;
	List<IState> states;
	TransMap transMap;
	IState initialState;
	IState endState;
	
	//DFA scope thread control.
	private IState sPtr;		//Current state pointer.
	private boolean isRunning;
	
	//Real case about implementation.
	IDfaLogic logicImpl;
	List<ILockedWaker> lockedWakers;
	ILockedWaker lwPtr;			//Current lockedWaker pointer.
	
	
	
	public LHDFA(String name){
		this.name = name;
		this.URI = name;
		this.parent = null;
		this.states = new ArrayList<IState>();
		this.transMap = new TransMap();
		//
		this.initialState = null;
		this.endState = null;
		this.sPtr = null;
		this.isRunning = false;
		this.logicImpl = null;
		
		this.lockedWakers = new ArrayList<ILockedWaker>();
		this.lwPtr = null;
	}
	
	/**
	 * Treat this LHDFA as top most DFA and start a thread to run it.
	 */
	public void start(){
		this.sPtr = this.initialState;
		this.isRunning = true;
		
		//
		Runnable run = new Runnable() {
			public void run() {
				//Routine for LHDFA itself.
				onEnter();
				activate();
				lock();
				onExit();
			}
		};
		
		Thread t = new Thread(run);
		t.start();
	}
	
	////////////////////////////
	// Child states about.
	////////////////////////////
	
	/**
	 * Add a child state. Set child's parent to this and also add stateName to transMap.
	 * Roll back state's parent assignment if same named state already exist.
	 * @param state state
	 * @return true if this collection changed as a result of the call.
	 */
	public boolean addState(IState state){
		//Set child state's child with this LHDFA, this enables the logic implementation binding.
		state.setParent(this);
		
		try {
			this.transMap.addState(state.getName());
		} catch (Exception e) {
			e.printStackTrace();
			//Undo parent assignment.
			state.setParent(null);
			return false;
		}
		
		return this.states.add(state);
	}
	
	/**
	 * Add a {state, trans}->nextState relation.
	 * Do nothing and output error msg if state doesn't exist or transition conflicted.
	 * @param stateName original state's name.
	 * @param transStr transition as string.
	 * @param nextStateName next state's name.
	 */
	public void addTransition(String stateName, String transStr, String nextStateName){
		try {
			this.transMap.addTrans(stateName, transStr, nextStateName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Set initial state.
	 * @param stateName initial state name.
	 */
	public void setInitailState(String stateName){
		this.initialState = this.findState(stateName);
	}
	
	/**
	 * Set end state.
	 * @param stateName end state name.
	 */
	public void setEndState(String stateName){
		this.endState = this.findState(stateName);
	}
	
	/**
	 * Find state registered by name.
	 * @param stateName
	 * @return state, null if not found.
	 */
	private IState findState(String stateName){
		for(IState state : this.states){
			if( state.getName().equals(stateName) ){
				return state;
			}
		}
		
		return null;
	}
	
	/**
	 * Set logic implementation of this LHDFA.
	 * @param logicImpl
	 */
	public void setLogicImpl(IDfaLogic logicImpl){
		this.logicImpl = logicImpl;
	}
	
	///////////////////////////
	// LockedWaker about
	///////////////////////////
	
	/**
	 * Register a lockedWaker.
	 * @param lw lockedWaker.
	 * @return true if operation successfully.
	 */
	public boolean addLockedWaker(ILockedWaker lw){
		return this.lockedWakers.add(lw);
	}
	
	/**
	 * Find registered lockedWaker by name.
	 * @param name target lockedWaker's name.
	 * @return lockedWaker, null if not found.
	 */
	public ILockedWaker findLockedWaker(String name){
		for(ILockedWaker lw : this.lockedWakers){
			if( lw.getName().equals(name) ){
				return lw;
			}
		}
		
		return null;
	}
	
	/**
	 * Set pointer of the current using lockedWaker.
	 * @param lw lockedWaker.
	 */
	public void setCurrentLockedWaker(ILockedWaker lw){
		this.lwPtr = lw;
	}
	
	/**
	 * Get pointer of current using lockedWaker.
	 * @return lockedWaker.
	 */
	public ILockedWaker getCurrentLockedWaker(){
		return this.lwPtr;
	}
	
	////////////////////////////////////
	// State about methods
	////////////////////////////////////
	
	@Override
	public String getName() {
		return this.name;
	}
	
	public String getURI(){
		return this.URI;
	}
	
	@Override
	public void setParent(LHDFA lhdfa) {
		this.parent = lhdfa;
		
		if( this.parent != null ){
			this.URI = this.parent.URI + "." + this.name;
		}else{
			this.URI = this.name;
		}
	}
	
	@Override
	public LHDFA getParent() {
		return this.parent;
	}

	@Override
	public void onEnter() {
		//System.out.println("[" + this.getURI() + "] onEnter");
	}
	
	@Override
	public ITransition activate() {
		if( this.parent != null ){
			//start child lhdfa node.
			this.isRunning = true;
			this.sPtr = this.initialState;
		}
		
		//Bind to logic implementation.
		ITransition lhdfaTrans = this.logicImpl.exec(this);
		
		/*
		 * Run all routines of target state, then if transition successfully, 
		 * do again for next state.
		 */
		while( this.isRunning ){
			//For state pointed by this.sPtr, activate it.
			this.sPtr.onEnter();
			ITransition sTrans = this.sPtr.activate();
			if( sTrans instanceof BreakTransition ){
				//Handle BreakTrans, toward this.lock() -> this.onExit().
				System.out.println("Break request by " + this.sPtr.getName());
				this.setCurrentLockedWaker(null);
				this.isRunning = false;
				break;
			}
			this.sPtr.lock();
			this.sPtr.onExit();
			
			//When work done,
			if( !this.sPtr.equals(this.endState) ){
				//1. If it's not the end state:
				
				try {
					//Extract transition and find next state.
					String nextStateName = this.transMap.getNextState(this.sPtr.getName(), sTrans.getName());
					IState nextState = this.findState(nextStateName);
					
					//Change pointer to the next state.
					this.sPtr = nextState;
					
				} catch (Exception e) {
					e.printStackTrace();
					//If transition fail, prepare to leave this level's LHDFA.
					//(Ignore last states to end state, then invoke lock() and onExit() 
					// of this LHDFA.)
					break;
				}
				
			}else{
				//2. If it's the end state:
				//Prepare to exit this LHDFA.
				this.isRunning = false;
			}
			
		}
		
		//return transition of the lhdfa.
		return lhdfaTrans;
		//return new DelayedTransition(this.getName()+"Exit", 0);
	}

	@Override
	public void lock() {
		//System.out.println("[" + this.getURI() + "] lock");
	}

	@Override
	public void onExit() {
		//System.out.println("[" + this.getURI() + "] onExit");
	}
	
	
	////////////////////////////////
	// Support Container Structure.
	////////////////////////////////
	
	/**
	 * Transition map for DFA.
	 * Define {transition string : next state name} for each state in the DFA.
	 * 
	 * @author Rahnoc
	 */
	class TransMap {
		//{stateName: {transStr: nextStateName}}
		Map<String, Map<String, String>> tMap;
		
		/**
		 * Constructor, initialize with an empty map.
		 */
		public TransMap(){
			this.tMap = new HashMap<String, Map<String, String>>();
		}
		
		/**
		 * Add a state to the map.
		 * @param stateName state's name.
		 * @throws Exception if the state name is used.
		 */
		public void addState(String stateName) throws Exception{
			if( !this.tMap.containsKey(stateName) ){
				Map<String, String> singleStateTM = new HashMap<String, String>();
				this.tMap.put(stateName, singleStateTM);
			}else{
				StringBuffer errSb = new StringBuffer();
				errSb.append("State named '" + stateName + "' already exists!");
				throw new Exception(errSb.toString());
			}
		}
		
		/**
		 * Add a transition pair {transition string : next state name} for target state.
		 * @param stateName the state to add transition pair on.
		 * @param transStr transition string
		 * @param nextStateName next state's name.
		 * @throws Exception if source state not exist or the transition is used. 
		 */
		public void addTrans(String stateName, String transStr, String nextStateName) throws Exception{
			if( this.tMap.containsKey(stateName) ){
				Map<String, String> singleStateTM = this.tMap.get(stateName);
				if(!singleStateTM.containsKey(transStr)){
					singleStateTM.put(transStr, nextStateName);
				}else{
					StringBuffer errSb = new StringBuffer();
					errSb.append("One transition with multiple next state in DFA!");
					errSb.append(System.lineSeparator());
					errSb.append("For " + stateName + " with transition " + transStr + ":");
					errSb.append("old " + singleStateTM.get(transStr));
					errSb.append(System.lineSeparator());
					errSb.append("new " + nextStateName);
					throw new Exception(errSb.toString());
				}
			}else{
				StringBuffer errSb = new StringBuffer();
				errSb.append("State '" + stateName + "' doesn't exists!");
				throw new Exception(errSb.toString());
			}
		}
		
		/**
		 * Get next state's name if apply the transition on target state.
		 * @param stateName original state's name.
		 * @param transStr transition string.
		 * @return next state's name.
		 * @throws Exception original state doesn't exist or
		 *  the transition is not using by the original state.
		 */
		public String getNextState(String stateName, String transStr) throws Exception{
			if(!this.tMap.containsKey(stateName)){
				throw new Exception("No state '" + stateName + "'");
			}
			
			Map<String, String> singleStateTM = this.tMap.get(stateName);
			if(!singleStateTM.containsKey(transStr)){
				throw new Exception("No legal transition '" + transStr + "' in " + stateName);
			}
			
			return this.tMap.get(stateName).get(transStr);
		}
		
		
		public String toString(){
			StringBuffer sb = new StringBuffer();
			for(Map.Entry<String, Map<String, String>> entry: this.tMap.entrySet()){
				String sState = entry.getKey();
				
				sb.append("State: " + sState);
				
				for(Map.Entry<String, String> tmEntry : entry.getValue().entrySet()){
					String transStr = tmEntry.getKey();
					String nState = tmEntry.getValue();
					
					sb.append(System.lineSeparator());
					sb.append(transStr).append(" -> ").append(nState);
				}
				
				sb.append(System.lineSeparator());
			}
			
			return sb.toString();
		}
		
	}
	
	
}
