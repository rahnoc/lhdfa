package exof.skylarkj.lhdfa;

/**
 * Timed locked waker implementation.
 * Using setTime() to set how long this waits (default 0 ms).
 * Invoke activate() then wait on object of this class to start waiting.
 * When timeout, this will notify the waiting thread.
 * 
 * Use insertKey() to force interrupt before timeout.
 * 
 * Invoke stop() will stop lw's thread, but state wait on this lw will keep blocked.
 * 
 * @author Rahnoc
 */
public class TimeLockedWaker implements ILockedWaker {
	String name;
	boolean isRunning;
	long deltaMs;
	Thread lwt;
	
	/**
	 * Constructor.
	 * @param name waker's name.
	 */
	public TimeLockedWaker(String name){
		this.name = name;
		this.deltaMs = 0;
		this.isRunning = false;
		this.lwt = null;
		
	}
	
	/**
	 * Set delay time.
	 * @param ms
	 */
	public void setTime(long ms){
		this.deltaMs = ms;
	}
	

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void activate(final Thread t) {
		this.isRunning = true;
		final Object so = this; //Synchronize object as this.
		
		//
		Runnable run = new Runnable() {
			public void run() {
				try {
					//Wait for delta milliseconds.
					Thread.sleep( deltaMs );
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if(isRunning){
					//Wake LHDFA.
					synchronized( so ){
						so.notify();
					}
				}
				
				//Stop lw.
				stop();
			}
		};
		
		this.lwt = new Thread(run);
		this.lwt.start();
	}

	@Override
	public void insertKey() {
		if( this.lwt != null && this.lwt.isAlive() ){
			this.lwt.interrupt();
		}
	}

	@Override
	public void stop() {
		this.isRunning = false;
		this.deltaMs = 0;
		this.lwt=null;
	}
	
	
}
