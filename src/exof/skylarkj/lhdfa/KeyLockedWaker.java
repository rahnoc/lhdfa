package exof.skylarkj.lhdfa;

/**
 * A multi-key slots lockedWaker implementation.
 * The state's lock() would wait on an instance of this waker.
 * 
 * After activation, this waker will start a new thread and wait others to insert keys.
 * When inserted keys are more or equal to slots of this waker, it notify the state 
 * lock by this waker.
 * 
 * Invoke stop() will stop lw's thread, but state wait on this lw will keep blocked.
 * 
 * @author Rahnoc
 */
public class KeyLockedWaker implements ILockedWaker {
	String name;
	boolean isRunning;
	Thread lwt;
	final int slots;	//How many keys needed.
	Keys keys;			//Keys this waker has.
	
	/**
	 * Constructor.
	 * @param name waker's name.
	 * @param slots needed slots, should be a positive integer.
	 */
	public KeyLockedWaker(String name, int slots){
		this.name = name;
		this.isRunning = false;
		this.slots = slots;
		this.lwt = null;
		
		try {
			//Holding zero keys in the first.
			this.keys = new Keys(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void activate(Thread t) {
		this.isRunning = true;
		final Keys keySo = this.keys;
		final Object so = this;
		
		//
		Runnable run = new Runnable() {
			public void run() {
				synchronized(keySo){
					try {
						//Wait insertion of keys.
						keySo.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				if(isRunning){
					//All keys acquired, notify the waited state.
					synchronized(so){
						so.notify();
					}
				}
				
				//Stop waker and throw all keys (set key num to 0).
				stop();
			}
		};
		
		this.lwt = new Thread(run);
		this.lwt.start();
	}

	@Override
	public void insertKey() {
		if( this.lwt!=null && this.lwt.isAlive()){
			//
			System.out.println("Insert key...");
			
			synchronized(this.keys){
				this.keys.increment();
				System.out.println("keys:" + this.keys + " / slots:" + this.slots);
				if( this.keys.getNum() >= this.slots){
					System.out.println("Unlock");
					this.keys.notify();
				}
			}
		}
	}

	@Override
	public void stop() {
		this.isRunning = false;
		//Clear keys.
		synchronized(this.keys){
			this.keys.notifyAll();
			this.keys.clear();
		}
		
		this.lwt = null;
	}
	
	/**
	 * Holding keys.
	 * @author Rahnoc
	 */
	public class Keys{
		private int num;
		
		/**
		 * Constructor, declare keys we have.
		 * @param num non-negative integer.
		 * @throws Exception if number of keys is negative.
		 */
		public Keys(int num) throws Exception{
			if( num < 0 ){
				throw new Exception("Key num: " + num + " negative!");
			}
			this.num = num;
		}
		
		/**
		 * Increase key number by 1.
		 */
		public void increment(){
			this.num++;
		}
		
		/**
		 * Decrease key number by 1. If last key number is zero, no change applied.
		 * The minimum key number is zero.
		 */
		public void decrement(){
			if( this.num > 0){
				this.num--;
			}
		}
		
		/**
		 * Clear number of keys by setting it to zero.
		 */
		public void clear(){
			this.num = 0;
		}
		
		/**
		 * Get number of keys.
		 * @return number of keys.
		 */
		public int getNum(){
			return this.num;
		}
		
		public String toString(){
			StringBuffer sb = new StringBuffer();
			sb.append(this.num);
			
			return sb.toString();
		}
	}
}
