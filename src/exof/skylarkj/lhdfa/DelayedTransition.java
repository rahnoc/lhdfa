package exof.skylarkj.lhdfa;

/**
 * Transition to indicate how long should wait in the lock().
 * @author Rahnoc
 */
public class DelayedTransition implements ITransition{
	String name;
	long delayMs;
	
	/**
	 * Constructor.
	 * @param name transition name.
	 * @param delay delay time in milliseconds.
	 */
	public DelayedTransition(String name, long delay){
		this.name = name;
		this.delayMs = delay;
	}

	@Override
	public String getName() {
		return this.name;
	}
	
	public String toString(){
		return DelayedTransition.class.getName() + ":" + this.name + "-" + this.delayMs + "ms.";
	}
}
