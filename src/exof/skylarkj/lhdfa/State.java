package exof.skylarkj.lhdfa;

/**
 * State of LHDFA.
 * @author Rahnoc
 */
public class State implements IState{
	String name;
	LHDFA parent;
	
	/**
	 * Constructor.
	 * @param name state name.
	 */
	public State(String name){
		this.name = name;
		this.parent = null;
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public void setParent(LHDFA lhdfa) {
		this.parent = lhdfa;
	}
	
	@Override
	public LHDFA getParent() {
		return this.parent;
	}

	@Override
	public void onEnter() {
		//System.out.println("[" + this.parent.getURI() + "]:" + this.getName() + " onEnter");
	}

	@Override
	public ITransition activate() {
		return this.parent.logicImpl.exec(this);
	}

	@Override
	public void lock() {
		//System.out.println("[" + this.parent.getURI() + "]:" + this.getName() + " lock");
		
		//Lock and activate lockedWaker.
		try {
			//Start corresponding LockedWaker.
			ILockedWaker lw = this.parent.getCurrentLockedWaker();
			lw.activate( Thread.currentThread() );
			
			//Lock and wait for lockedWaker.
			synchronized(lw){
				lw.wait();
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void onExit() {
		//Clear lockedWaker pointer.
		this.parent.setCurrentLockedWaker(null);
		
		//System.out.println("[" + this.parent.getURI() + "]:" + this.getName() + " onExit");
	}

}
