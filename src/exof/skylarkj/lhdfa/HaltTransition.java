package exof.skylarkj.lhdfa;

/**
 * Transition stands for halting in the lock() od a state.
 * @author Rahnoc
 */
public class HaltTransition implements ITransition{
	String name;
	
	/**
	 * Constructor.
	 * @param name transition name.
	 */
	public HaltTransition(String name){
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}
}
