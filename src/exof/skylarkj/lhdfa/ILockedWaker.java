package exof.skylarkj.lhdfa;

/**
 * Interface of the LockedWaker.
 * When activate, lw is start to waiting for others to insert keys.
 * After collecting enough keys should this LockedWaker to wake up the target monitoring
 * (usually a thread blocked in a state's lock method).
 * 
 * @author Rahnoc
 */
public interface ILockedWaker {
	String getName();
	
	/**
	 * Activate lockedWaker.
	 * @param t thread wait for this waker.
	 */
	void activate(Thread t);
	
	/**
	 * Insert a key.
	 */
	void insertKey();
	
	/**
	 * Stop lockedWaker.
	 */
	void stop();
}
